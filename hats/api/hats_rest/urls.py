from django.urls import path
from .views import hat_list, api_show_hat

urlpatterns = [
    path("hats/", hat_list, name="api_create_hats"),
    path("locations/<int:location_vo_id>/hats/", hat_list, name="api_list_hats"),
    path("hats/<int:pk>/", api_show_hat, name="api_show_hat")
]
