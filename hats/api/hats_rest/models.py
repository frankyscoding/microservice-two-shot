from django.db import models
from django.urls import reverse

class LocationVO(models.Model):
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField(null=True)
    shelf_number = models.PositiveSmallIntegerField(null=True)
    import_href = models.CharField(max_length=200, unique=True)

class Hat(models.Model):
    style = models.CharField(max_length=200)
    fabric = models.CharField(max_length=200, null=True)
    color = models.CharField(max_length=200, null=True)

    picture_url = models.URLField(null=True)

    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE,
        null=True
    )


    def __str__(self):
        return self.style

    def get_api_url(self):
        return reverse("api_show_hat", kwargs={"pk": self.pk})

# Create your models here.
