import React from 'react'
import { HashRouter } from 'react-router-dom'
import { NavLink } from 'react-router-dom';



function HatList({ hats, getHats }) {
    const deleteHat = async (id) => {
        fetch(`http://localhost:8090/api/hats/${id}/`, {
            method: "delete",
        })
        .then(() => {
            return getHats()
        })
        .catch(console.log)

    }
        if (hats === undefined) {
            return null
    }
  return (
    <>
    <div>
        <button><NavLink className="navbar-brand" to="/hats/new">Create a Hat</NavLink></button>
    </div>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Style</th>
          <th>Fabric</th>
          <th>Color</th>
          <th>Picture</th>
          <th>Location</th>
          <th>Delete</th>
        </tr>
      </thead>
      <tbody>
        {hats.map(hat => {
          return (
            <tr key={hat.id}>
              <td>{ hat.style }</td>
              <td>{ hat.fabric }</td>
              <td>{ hat.color }</td>
              <td>{ hat.picture_url }</td>
              <td>{ hat.location }</td>
              <td>
                <button type="button" value={hat.id} onClick= {() => deleteHat(hat.id)}>Delete</button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
    </>
  )
}

export default HatList
