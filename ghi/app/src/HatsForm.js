import React, {useEffect, useState } from 'react';


function HatsForm() {
    const [locations, setLocations] = useState([])
    const [style, setStyle] = useState('')
    const [fabric, setFabric] = useState('')
    const [color, setColor] = useState('')
    const [picture_url, setPicture] = useState('')
    const [location, setLocation] = useState('')

    const stylechange = (event) => {
        const value = event.target.value;
        setStyle(value);
      }

    const fabricchange = (event) => {
        const value = event.target.value;
        setFabric(value);
      }

    const colorchange = (event) => {
        const value = event.target.value;
        setColor(value);
      }

    const picturechange = (event) => {
        const value = event.target.value;
        setPicture(value);
      }

    const locationchange = (event) => {
        const value = event.target.value;
        setLocation(value);
      }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          setLocations(data.locations)


        }
      }

      useEffect(() => {
        fetchData();
      }, []);

    const submitchange = async (event) => {
        event.preventDefault();

        const data = {}
        data.style = style
        data.fabric = fabric
        data.color = color
        data.picture_url = picture_url
        data.location = location

        console.log(data)
        const hatUrl = 'http://localhost:8090/api/hats/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }

        const response = await fetch(hatUrl, fetchConfig)
        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat);

            setStyle('');
            setFabric('');
            setColor('');
            setPicture('');
            setLocation('');
        }
    }


  return (
    <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a hat</h1>
            <form onSubmit={submitchange} id="create-hat-form">
              <div className="form-floating mb-3">
                <input onChange={stylechange} placeholder="Style" required type="text" name ="style" id="style" className="form-control" value={style}/>
                <label htmlFor="name">Style</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={fabricchange} placeholder="Fabric" required type="text" name ="fabric" id="fabric" className="form-control" value={fabric}/>
                <label htmlFor="start">Fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={colorchange} placeholder="Color" required type="text" name ="color" id="color" className="form-control" value={color}/>
                <label htmlFor="end">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={picturechange} placeholder="Picture" required type="url" name ="picture_url" id="picture_url" className="form-control" value={picture_url}/>
                <label htmlFor="picture">Picture</label>
              </div>
              <div className="mb-3">
                <select onChange={locationchange} required id="location" name="location" className="form-select" value={location}>
                <option value="">Choose a location</option>
                {locations.map(locationss => {
                    return (
                        <option key={locationss.href} value={locationss.href}>
                            {locationss.closet_name}
                        </option>
                );
                })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
  )
}

export default HatsForm
