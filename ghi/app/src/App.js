import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import React, {useEffect, useState } from 'react';
import HatList from './HatList';
import HatsForm from './HatsForm';




function App() {

const [hats, setHats] = useState([])

const getHats = async () => {
  const url = 'http://localhost:8090/api/hats/'
  const response = await fetch(url);

  if (response.ok) {
    const data = await response.json();
    console.log(data)
    const hats = data.hats
    setHats(hats)
  }
}




  useEffect(() => {
    getHats();
  }, []);

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats">
            <Route index element={<HatList hats={hats} getHats={getHats} />}/>
            <Route path="new" element={<HatsForm/>} />
        </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
